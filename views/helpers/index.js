module.exports = (hbs) => {
    hbs.registerHelper("phone_number_bolder", (phone_number) => {
        if(phone_number.match(/[0-9]/g).length > 10) {
            return `<b>${phone_number}</b>`
        }
        return phone_number
    })

    hbs.registerHelper("company_highlight", (company) => {
        console.log(company)
        if(company === 'Microsoft') {
            return 'background-color: #00FF00'
        }
        return ''
    })

    return hbs;
}